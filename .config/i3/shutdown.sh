#!/bin/bash

# 👢 💀 💩 📡 🔒 🕺 🛵 🚷 🤠 🔒 🔓 🔏 🔐 🔑 🛡
xmodmap -e "keycode 114 = Right"
xmodmap -e "keycode 113 = Left"

res=$(echo  "  🕺;  👢;  💀;  🔒" | rofi -sep ";" -dmenu -p '' -font 'Ubuntu 80'  \
  -lines 1 -columns 4 -location 0 -separator-style solid -width 990 -hide-scrollbar -padding 10)

if   [[ $res = "  🕺" ]]; then
    i3-msg exit
elif [[ $res = "  👢" ]]; then
    systemctl reboot; reboot
elif [[ $res = "  💀" ]]; then
    systemctl poweroff
elif [[ $res = "  🔒"   ]]; then
    ~/.config/i3/lock.sh
else
  exit 0
fi

