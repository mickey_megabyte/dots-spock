  syntax on
  set hidden
  set mouse=a
  " colo aq257mute
  colo seagr-aq
  set noshowmode
  set helpheight=99
  set virtualedit=block
  set splitbelow splitright
  set number numberwidth=5 rnu
  set termguicolors background=dark
  set updatetime=800 ttimeoutlen=60
  set clipboard^=unnamed,unnamedplus
  set cursorline scrolloff=9 nostartofline

  set linebreak
  set copyindent
  filetype plugin on
  let &showbreak='⤷ '
  set fillchars=vert:\ 
  filetype plugin indent on
  set wrap whichwrap+=<,>,[,]
  set shiftround shiftwidth=2
  set tabstop=2 expandtab softtabstop=2
  set list listchars=tab:‥\ ,trail:˙,nbsp:␣
  set foldmethod=indent foldenable foldlevelstart=1

  set path+=**
  set gdefault showmatch ignorecase
  set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.pdf,*/Music/*,*/Pictures/*


  let g:GuiInternalClipboard = 1



  " restore cursor position upon reopening files
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif


"... make folds a wee bit more interesting
  function! MyFoldText()
    let foldsize = (v:foldend-v:foldstart)
    return '◾ '.getline(v:foldstart).'     ◾      ('.foldsize.' lines)        '. repeat('◾          ', 9)
  endfunction
  set foldtext=MyFoldText()


  " set swapfile
  " set directory=/home/angelo/swap

""---   backup/undo
 " if exists('$SUDO_USER')         " don't create root-owned `files
 "   set nobackup nowritebackup noswapfile noundofile
 " else                            " keep files out of the way
 "   set backup undofile
 "   set directory=/home/angelo/.config/nvim/swap//
 " endif

 "     set directory=/home/angelo/swap//

