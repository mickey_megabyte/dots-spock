"  .. settings for pluginz

"........  misc  ..............
  let g:vimwiki_folding='syntax'
  let g:vimwiki_hl_cb_checked = 1
  let g:vimwiki_listsyms = 'ﯧﭦﭥ●'
  let g:vimwiki_listsym_rejected = ''

  nmap <Leader>wn <Plug>VimwikiNextLink
  nmap <Leader>t <Plug>VimwikiToggleRejectedListItem




  hi StartifyPath guifg=#227722
  let g:startify_files_number = 5
  let g:startify_enable_special = 0
  autocmd User Startified setlocal cursorline
  " let g:startify_bookmarks = [{'v':'~/.vimrc'}]
  let g:startify_custom_header = map(startify#fortune#boxed(), '"    ".v:val')



  let g:user_emmet_install_global = 0
  " let g:user_emmet_leader_key='<C-M>'
  autocmd FileType html,css EmmetInstall



  let g:SexyScroller_MaxTime = 200
  let g:SexyScroller_EasingStyle = 2



  let g:indentLine_char = '⁚'
  let g:indentLine_enabled = 0
  let g:indentLine_bgcolor_gui = '#1d2428'



"... Goyo and limelight  .....
  let g:goyo=0
  let goyo_width=110
  let goyo_height=95
  let g:limelight_paragraph_span = 2

  function! s:goyo_enter()
    let g:goyo = 1
    set showtabline=0
  endfunction

  function! s:goyo_leave()
    let g:goyo = 0
    hi iCursor guifg=red guibg=yellow
  endfunction

  autocmd! User GoyoLeave nested call <SID>goyo_leave()
  autocmd! User GoyoEnter nested call <SID>goyo_enter()



" ___________lightline__________________________________
  let g:lightline = {
      \ 'colorscheme': 'materia',
      \ 'component_function': {
      \   'gitbranch': 'gitbranch#name',
      \   'filetype': 'MyFiletype',
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' },
      \ 'active': {
      \   'left': [['mode', 'paste'], ['gitbranch'], ['buffers']] ,
      \   'right': [['filetype', 'percent', 'lineinfo']],
      \ },
      \ 'inactive': {},
  \ }

  function! MyFiletype()
    return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype . ' ' . WebDevIconsGetFileTypeSymbol() : 'no ft') : ''
  endfunction

  let g:lightline#bufferline#modified  = '😱 '
  let g:lightline#bufferline#read_only  = '🔒 '
  let g:lightline#bufferline#show_number  = 0    " if 2, show nums from 1
  let g:lightline.component_type   = {'buffers': 'tabsel'}
  let g:lightline.component_expand = {'buffers': 'lightline#bufferline#buffers'}
  let g:lightline.tabline          = {'left': [['buffers']], 'right': [['close']]}

  function! LightlineUpdateAQ()
    if g:goyo==0
      call lightline#update()
    endif
  endfunction

  au BufWritePost,TextChanged,TextChangedI * call LightlineUpdateAQ()

  nmap <Leader>1 <Plug>lightline#bufferline#go(1)
  nmap <Leader>2 <Plug>lightline#bufferline#go(2)
  nmap <Leader>3 <Plug>lightline#bufferline#go(3)
  nmap <Leader>4 <Plug>lightline#bufferline#go(4)
  nmap <Leader>5 <Plug>lightline#bufferline#go(5)
  nmap <Leader>6 <Plug>lightline#bufferline#go(6)



".... NERDTree
  " NERDTree File highlighting
  function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
    exec 'au FileType nerdtree hi ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
    exec 'autocmd FileType nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
  endfunction

  call NERDTreeHighlightFile('vue', 'green', 'none', 'green', 'NONE')
  call NERDTreeHighlightFile('ini', 'yellow', 'none', '#771199', 'NONE')
  call NERDTreeHighlightFile('md', 'blue', 'none', '#3366aa', 'NONE')
  call NERDTreeHighlightFile('html', 'yellow', 'none', '#994499', 'NONE')
  call NERDTreeHighlightFile('styl', 'cyan', 'none', 'cyan', 'NONE')
  call NERDTreeHighlightFile('css', 'cyan', 'none', 'cyan', 'NONE')

  " . . .  dunno if worx? . .  dunno wot it does?
  function! s:attempt_select_last_file()
    let l:previous=expand('#:t')
    if l:previous != ''
      call search('\v<' . l:previous . '>')
    endif
  endfunction

  augroup NERDTree
    autocmd!
    autocmd User NERDTreeInit call s:attempt_select_last_file()
  augroup END

  autocmd User NERDTreeInit call s:attempt_select_last_file()

  " Move up a directory using "-" like vim-vinegar (usually "u" does this).
  nmap <buffer> <expr> - g:NERDTreeMapUpdir
  " Like vim-vinegar.
  nnoremap <silent> - :silent edit <C-R>=empty(expand('%')) ? '.' : expand('%:p:h')<CR><CR>

  augroup nerdtreehidecwd           " remove slashes from NERDTree
    autocmd!
    au FileType nerdtree setlocal conceallevel=3 | syntax match NERDTreeDirSlash #/$# containedin=NERDTreeDir conceal contained
  augroup end

  let NERDTreeMinimalUI=1
  let NERDTreeQuitOnOpen=1
  let NERDTreeShowBookmarks=1
  let NERDTreeBookmarksFile=expand("$HOME/.config/nvim/NERDTreeBookmarks")

  let g:netrw_banner = 0
  let g:netrw_winsize = 25
  let g:netrw_liststyle = 3

