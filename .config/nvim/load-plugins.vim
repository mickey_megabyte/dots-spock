" -------  load da pluginz

if empty(glob('~/.config/nvim/autoload/plug.vim'))
    silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif

  call plug#begin('~/.config/nvim/plugged')
    Plug 'airblade/vim-gitgutter'
    Plug 'ap/vim-css-color'
    Plug 'blueyed/vim-diminactive'
    Plug 'digitaltoad/vim-pug'
    Plug 'hail2u/vim-css3-syntax'
    Plug 'iloginow/vim-stylus'
    Plug 'itchyny/lightline.vim'
    Plug 'itchyny/vim-gitbranch'
    Plug 'joeytwiddle/sexy_scroller.vim'
    Plug 'junegunn/fzf.vim'
    Plug 'junegunn/goyo.vim'
    Plug 'junegunn/limelight.vim'
    Plug 'junegunn/fzf', { 'dir': '~/.config/fzf', 'do': './install --all' }
    Plug 'mattn/emmet-vim'
    Plug 'mattn/calendar-vim'
    " Plug 'maxboisvert/vim-simple-complete'
    Plug 'mgee/lightline-bufferline'
    Plug 'mhinz/vim-startify'
    Plug 'pangloss/vim-javascript'
    Plug 'ryanoasis/vim-devicons'
    Plug 'scrooloose/nerdtree'
    Plug 'storyn26383/vim-vue'
    Plug 'tpope/vim-commentary'
    Plug 'vimwiki/vimwiki'
    Plug 'PotatoesMaster/i3-vim-syntax'
    Plug 'Yggdroot/indentLine'
  call plug#end()

